(defpackage cryptutils/tests/main
  (:use :cl
        :cryptutils
        :rove))
(in-package :cryptutils/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :cryptutils)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
