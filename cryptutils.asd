(defsystem "cryptutils"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ("bit-foolery" "ironclad" "amb")
  :components ((:module "src"
                :components
                ((:file "utils")
                 (:file "math")
                 (:file "prng")
                 (:file "io")
                 (:file "frequencies")
                 (:file "main")
                 (:file "oracles"))))

                 
  :description ""
  :in-order-to ((test-op (test-op "cryptutils/tests"))))

(defsystem "cryptutils/tests"
  :author ""
  :license ""
  :depends-on ("cryptutils"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cryptutils"
  :perform (test-op (op c) (symbol-call :rove :run c)))
