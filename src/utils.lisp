(defpackage cryptutils/utils
  (:use :cl)
  (:export
   :alist-key-union
   :longer-list
   :shorter-list
   :list-head
   :list-tail
   :max-length
   :sequencep
   :factorial-pairs
   :split-string
   :unsplit-string
   :strip-chars
   :sub-string-position
   :length-of-string-stream
   :read-number-to-string))

(in-package :cryptutils/utils)

(defun alist-key-union (list1 list2)
  "Return a list of all the keys used in both lists, but only one instance of each key."
  (let ((result '()))
    (loop for (key . val) in list1
          do (setf result (adjoin key result)))
    (loop for (key . val) in list2
          do (setf result (adjoin key result)))
    result))

(defun longer-list (list1 list2)
  "Return whichever list is longer."
  (if (>= (length list1) (length list2))
      list1
      list2))

(defun shorter-list (list1 list2)
  "Return whichever list is shorter."
  (if (< (length list1) (length list2))
      list1
      list2))

(defun list-head (conses-to-keep list)
  "Return the start of the list with the given number of conses."
  (cond ((>= conses-to-keep (length list))
         list)
        ((> conses-to-keep 1)
         (let ((new-list (copy-list list)))
           (rplacd (nthcdr (1- conses-to-keep) new-list) nil)
           new-list))
        (t nil)))

(defun list-tail (conses-to-keep list)
  "Return the end of the list with the given number of conses."
  (cond ((>= conses-to-keep (length list))
         list)
        ((> conses-to-keep 1)
         (nthcdr (- (length list) conses-to-keep) list))
        (t nil)))

(defun max-length (list)
  "Return the length of the longest sequence in the list."
  (loop for entry in list
        maximizing (length entry)))

(defun sequencep (object)
  "Is the object a sequence? Sequences are lists, strings, and vectors."
  (or (listp object)
      (stringp object)
      (and (arrayp object)
           (= 1 (array-rank object)))))

(defun factorial-pairs (list)
  "Return a list of lists, where each sub-list is a set of size 2, and has a unique combination of
   the contents of the original list. Eg of size 2, '(1 2 3) => '((1 2) (1 3) (2 3))"
  (loop with result = '()
        with length = (length list)
        for primary from 0 to (- length 2)
        do (loop for secondary from (+ 1 primary) to (1- length)
                 do (push (list (nth primary list) (nth secondary list)) result))
        finally (return (nreverse result))))

(defun split-string (string target)
  (let ((length (length target)))
    (loop with previous-index = 0
          for index from 1 below (- (length string) length)
          when (string= (subseq string index (+ index length))
                        target)
            collecting (subseq string previous-index index) into list
            and do (setf previous-index (+ index length))
          finally (return (append list (list (subseq string previous-index)))))))

(defun unsplit-string-list (list target)
  (loop with unsplit = '()
        for item in (butlast list)
        do (push item unsplit)
           (push target unsplit)
        finally (return (string-list-to-string (append (nreverse unsplit) (last list))))))

(defun strip-chars (string &rest chars)
  "Return a string without the chars present."
  (loop for char across string
        unless (find char chars :test #'char=)
          collecting char into result
        finally (return (coerce result 'string))))

(defun substring-position (substring string)
  "Return the index of the first instance of substring within string where the whole substring
  matches."
  (let ((sub-length (length substring)))
    (loop for index from 0 to (- (length string) sub-length)
          when (string= string substring :start1 index :end1 (+ index sub-length)
                                         :start2 0 :end2 sub-length)
            do (return index))))

(defun length-of-string-stream (stream)
  "Return the total count characters in this stream of strings."
  (loop for line = (read-line stream nil)
        while line
        summing (length line)))

(defun string-list-to-string (list)
    "Coerce the list of strings to be one big string maintaining positions in the list."
  (let ((length (reduce (lambda (x y) (+ (if (sequencep x) (length x) x)
                                         (if (sequencep y) (length y) y)))
                        list)))
    (loop with output = (make-string length)
          with index = 0
          for string in list
          do (loop for entry across string
                   do (setf (aref output index) entry)
                      (incf index))
          finally (return output))))
          
(defun read-number-to-string (number)
  "Return the string the number represents as if the number was 8-bit width characters."
  (loop with num-length = (ceiling (log number 2))
        with string-length = (ceiling num-length 8)
        with output = (make-string string-length)
        for string-index from (1- string-length) downto 0
        for num-index from 0 to num-length by 8
        do (setf (char output string-index)
                 (code-char (ldb (byte 8 num-index) number)))
        finally (return output)))

