(defpackage cryptutils/math
  (:use :cl)
  (:import-from :bit-foolery
                :shift-high)
  (:export
   :min-frame-width
   :round-down-to-mod
   :round-up-to-mod
   :round-to-mod
   :mod-expt
   :parse-string))

(in-package :cryptutils/math)


(defun min-frame-width (number base)
  "The minimum string size needed to print the number in the base."
  (if (zerop number)
      1
      (ceiling (log (1+ number) base))))

(defun round-down-to-mod (number mod)
  "Round the number to the mod=0 number below it."
  (* mod (floor number mod)))

(defun round-up-to-mod (number mod)
  "Round the number to the mod=0 number above it."
  (+ mod (round-down-to-mod number mod)))


(defun round-to-mod (number mod)
  "Round to the nearest mod=0 above or equal to the number."
  (let ((rem (mod number mod)))
    (if (zerop rem)
        number
        (+ number mod (- rem)))))

(defun mod-expt (base expt mod)
  "Calculate the result of raising the base to the exponent then mod'ing."
  ;; This is fast because rather than calculate the exponent then mod, it mods while calculating
  ;; So the intermediate numbers never grow out of hand
  ;; (a * b) mod c == ((a mod c) * (b mod c)) mod c
  (loop for pos from 0 below (integer-length expt)
        for n = (ldb (byte 1 pos) expt)
        for tmp = base then (mod (* tmp tmp) mod)
        with result = 1
        unless (zerop n)
          do (setf result (mod (* tmp result) mod))
        finally (return result)))


(defun parse-string (string)
  "Return the appropriate integer from reading the string. Like parse-int if print-base was 256.
  Treats all characters as a b256 digit."
  (loop with length = (length string)
        with result = 0
        for index from (1- length) downto 0
        for char across string
        do (setf result (logior result (shift-high (char-code char)
                                                   (* length 8)
                                                   (* index 8))))
        finally (return result)))
