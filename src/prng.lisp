(defpackage cryptutils/prng
  (:use :cl)
  (:import-from :bit-foolery
                :shift-high
                :shift-low)
  (:export
   :make-prng
   :seed-prng
   :extract-number))

(in-package :cryptutils/prng)

(defvar *word-size* 32)
(defvar *degree* 624)
(defvar *middle-word* 397)
(defvar *seperation-point* 31)
(defvar *coefficients*  #x9908B0DF)
(defvar *temper-mask-1* #xFFFFFFFF)
(defvar *temper-mask-2* #x9D2C5680)
(defvar *temper-mask-3* #xEFC60000)
(defvar *temper-shift-1* 11)
(defvar *temper-shift-2* 7)
(defvar *temper-shift-3* 15)
(defvar *temper-shift-4* 18)
(defvar *initialization-seed* 1812433253)

(defvar upper-mask (1- (ash *seperation-point* 1)))
(defvar lower-mask (ldb (byte *word-size* 0) (lognot upper-mask)))

(deftype prng-int ()
  (list 'integer 0 (1- (expt 2 *word-size*))))

(deftype prng-index ()
  (list 'integer 0 *degree*))

(defstruct (prng (:constructor %make-prng))
 (index 0 :type prng-index)
 (seed 0 :type prng-int)
 (seeded nil :type boolean)
 (array (make-array *degree* :element-type 'prng-int
                           :adjustable nil
                           :fill-pointer nil)))

(defun make-prng (seed)
  (%make-prng :seed seed))

(defun seed-prng (prng)
  (setf (aref (prng-array prng) 0)
        (prng-seed prng))
  (loop for index from 1 below *degree*
        do (setf (aref (prng-array prng) index)
                 (ldb (byte *word-size* 0)
                      (+ index
                         (* *initialization-seed*
                            (logxor (aref (prng-array prng) (1- index))
                                 (ash (aref (prng-array prng) (1- index))
                                      (- 2 *word-size*))))))))
  (setf (prng-seeded prng) t)
  prng)

(defun twist (prng)
  (loop for index from 0 below *degree*
        do (let* ((x (logior (logand (aref (prng-array prng) index)
                                     upper-mask)
                             (logand (aref (prng-array prng) (mod (1+ index) *degree*))
                                     lower-mask)))
                  (xA (if (zerop (mod x 2))
                          (logxor (shift-high x *word-size* 1)
                                  *coefficients*)
                          (shift-high x *word-size* 1))))
             (setf (aref (prng-array prng) index)
                   (logxor (aref (prng-array prng) (mod (+ index *middle-word*) *degree*))
                           xA))))
  (setf (prng-index prng) 0)
  prng)

(defun extract-number (prng)
  (assert (prng-seeded prng))
  (when (>= (prng-index prng) *degree*)
    (twist prng))
  (let* ((y (aref (prng-array prng) (prng-index prng)))
         (y (logxor y (logand (shift-low y *word-size* *temper-shift-1*)
                              *temper-mask-1*)))
         (y (logxor y (logand (shift-high y *word-size* *temper-shift-2*)
                              *temper-mask-2*)))
         (y (logxor y (logand (shift-high y *word-size* *temper-shift-3*)
                              *temper-mask-3*)))
         (y (logxor y (shift-low y *word-size* *temper-shift-4*))))
    (incf (prng-index prng))
    (ldb (byte *word-size* 0) y)))

