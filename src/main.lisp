(defpackage cryptutils
  (:use :cl)
  (:import-from :ironclad
                :encrypt
                :decrypt)
  (:import-from :cryptutils/io
                :make-byte-vect
                :copy-byte-vect-from
                :copy-byte-vect-into
                :copy-byte-vect-range
                :byte-vect-to-string
                :read-b64-file-to-byte-vect
                :string-to-byte-vect
                :trim-byte-vect
                :byte-vect-to-string
                :read-string-file-to-byte-vect
                :transpose-chunks
                :list-to-byte-vect
                :print-byte-vect-as-hex
                :hex-string-to-byte-vect
                :transpose-byte-vects)
  (:import-from :cryptutils/math
                :round-up-to-mod
                :parse-string)
  (:import-from :cryptutils/utils
                :list-head
                :factorial-pairs)
  (:import-from :cryptutils/frequencies
                :select-most-likely-english)
  (:export
  :byte-vect-xor
  :hex-string-xor
  :attempt-decode-xor-against-one-repeated-char
  :xor-octal-against-byte-vect
  :attempt-decode-xor-against-one-repeated-char-file
  :repeating-key-xor
  :repeating-key-vect
  :hamming-string-distance
  :hamming-vect-distance
  :hamming-int-distance
  :attempt-decode-xor-against-repeat
  :find-edit-distance
  :decrypt-aes-ecb
  :likely-aes-ecb-strings-in-file
  :likely-aes-ecb-string
  :pad-vect-pkcs7
  :unpad-vect-pkcs7
  :valid-pkcs7
  :encrypt-string-file-to-aes-cbc-to-b64
  :encrypt-aes-cbc
  :decrypt-aes-cbc
  :decrypt-b64-file-aes-cbc
  :encrypt-aes-ecb
  :encrypt-aes-ecb-padded
  :next-pad-size
  :decrypt-aes-ecb-padded
  :pad-vect-pkcs7-to-block
  :encrypt-aes-ctr
  :decrypt-aes-ctr
  :attempt-decode-xor-repeating-key-vect-list))

(in-package :cryptutils)

;;;; Code developed while solving cryptopals puzzles.

(defun byte-vect-xor (vect1 vect2)
  "XOR the two byte vectors against eachother. If one is longer then keep the tail of it
  in the result."
  (loop with length1 = (length vect1)
        with length2 = (length vect2)
        with new-vect = (make-byte-vect (max length1 length2))
        with length = (min length1 length2)
        for index from 0 below length
        do (setf (aref new-vect index) (logxor (aref vect1 index) (aref vect2 index)))
        finally (when (/= length1 length2)
                  (let ((longer-vect (first (sort (list vect1 vect2)
                                                  (lambda (x y) (> (length x) (length y)))))))
                    (loop for tail-index from index below (length longer-vect)
                          do (setf (aref new-vect tail-index) (aref longer-vect tail-index)))))
                (return new-vect)))

(defun hex-string-xor (string1 string2 &optional (stream *standard-output*))
  "XOR the two hex strings against eachother. Must be same length. If not equal length then
  convert to byte-vectors and use that."
  (assert (= (length string1) (length string2)))
  (let ((*print-base* 16))
    (princ (logxor (parse-integer string1 :radix 16)
                   (parse-integer string2 :radix 16))
           stream)))

(defun attempt-decode-xor-against-one-repeated-char (input)
  "Return the most likely english decoding of xoring the double-hex string against another string
   filled with double hex chars."
    ;; (format t "Attempting encoding ~A:~%" (print-byte-vect-as-string input))
    (loop for keycode from 0 to 255
          for key-vect = (make-byte-vect (length input) :initial-element keycode)
          for encoded = (byte-vect-xor input key-vect)
          ;; do (format t "Encoded to ~A using key ~A~%" (print-vect-as-string encoded nil)
          ;;            (code-char keycode))
          collecting (cons encoded keycode) into encoded-list
          finally (let ((most-likely (select-most-likely-english encoded-list)))
                    (return (values (first most-likely) (rest most-likely))))))

(defun xor-octal-against-byte-vect (vect octal)
  "Return the result of xoring the octal against all of the bytes in the vect."
  (loop with encoded = (make-byte-vect (length vect))
        for entry across vect
        for index from 0
        do (setf (aref encoded index) (logxor entry octal))
        finally (return encoded)))

(defun attempt-decode-xor-against-one-repeated-char-file (path)
  "Attack the file assuming it is encoded with repeating-char XOR."
  (with-open-file (stream path)
    (loop for string = (read-line stream nil)
          while string
          collecting (cons (attempt-decode-xor-against-one-repeated-char
                            (hex-string-to-byte-vect string))
                           nil)
            into best-list
          finally (return (first (select-most-likely-english best-list))))))

(defun repeating-key-xor (key string)
  "Encrypt the string using the key"
  (print-byte-vect-as-hex (byte-vect-xor (string-to-byte-vect string)
                                         (repeating-key-vect (string-to-byte-vect key)
                                                             (length string)))
                          nil))

(defun repeating-key-vect (key-vect length)
  "Make a vector with the key string repeating as the contents."
  (loop with vect = (make-byte-vect length)
        for index from 0 below length
        for key-index = (mod index (length key-vect))
        do (setf (aref vect index) (aref key-vect key-index))
        finally (return vect)))

(defun hamming-string-distance (string1 string2)
  "Measure the hamming distance between the strings."
  (let* ((int1 (parse-string string1))
         (int2 (parse-string string2)))
    (loop for index from 0 to (max (log int1 2) (log int2 2))
          counting (/= (ldb (byte 1 index) int1) (ldb (byte 1 index) int2)) into difference
          finally (return difference))))

(defun hamming-vect-distance (byte-vect1 byte-vect2)
  "Measure the hamming distance between the byte vectors."
  (let ((length1 (length byte-vect1))
        (length2 (length byte-vect2)))
    (loop for index from 0 below (min length1 length2)
          summing (hamming-int-distance (aref byte-vect1 index) (aref byte-vect2 index)) into sum
          finally (return (+ sum (* (abs (- length1 length2))
                                    8))))))

(defun hamming-int-distance (int1 int2)
  "Measure the hamming distance between the integers."
  (loop for index from 0 to (max (integer-length int1) (integer-length int2))
        counting (/= (ldb (byte 1 index) int1) (ldb (byte 1 index) int2))))

(defun attempt-decode-xor-against-repeat (vect &optional (max-keysize 40))
  "Attack the vector, assuming it is an english string encoded with a repeating character xor."
  (loop for keysize from 2 to max-keysize
        collecting (cons (find-edit-distance vect keysize) keysize) into distances
        finally (return
                  (loop for keysize in (list-head 4 (sort distances #'< :key #'first))
                        collecting (loop for chunk in (transpose-chunks vect (rest keysize))
                                         collecting
                                         (multiple-value-bind (vect key)
                                             (attempt-decode-xor-against-one-repeated-char
                                              chunk)
                                           (declare (ignore vect))
                                           key)
                                         into keys
                                         finally (return
                                                   (list (byte-vect-xor
                                                          vect
                                                          (repeating-key-vect
                                                           (list-to-byte-vect keys)
                                                           (length vect))))))
                          into results
                        finally (return (select-most-likely-english results))))))

(defun find-edit-distance (vect keysize)
  "Return the edit distance of the vector. A lower result means the keysize is likely the length
  of a repeating key XOR used to encode the vector."
  (assert (> (length vect) (* keysize 4)))
  (let ((chunk0 (subseq vect 0 keysize))
        (chunk1 (subseq vect keysize (* keysize 2)))
        (chunk2 (subseq vect (* keysize 2) (* keysize 3)))
        (chunk3 (subseq vect (* keysize 3) (* keysize 4))))
    (loop for (chunk-a chunk-b) in (factorial-pairs (list chunk0 chunk1 chunk2 chunk3))
          summing (hamming-vect-distance chunk-a chunk-b) into sum
          finally (return (float (/ (/ sum 6) keysize))))))

(defun decrypt-aes-ecb (in-vect key-vect)
  "Decrypt the in-vect byte vector using the key-vect as AES ECB."
  (let ((cipher (ironclad:make-cipher :aes :key key-vect :mode :ecb))
        (out-vect (make-byte-vect (length in-vect))))
    (decrypt cipher in-vect out-vect)
    out-vect))

(defun encrypt-aes-ecb (in-vect key-vect)
  "Encrypt the in-vect byte vector using the key-vect as AES ECB."
  (let ((cipher (ironclad:make-cipher :aes :key key-vect :mode :ecb))
        (out-vect (make-byte-vect (length in-vect))))
    (encrypt cipher in-vect out-vect)
    out-vect))

(defun likely-aes-ecb-strings-in-file (path)
  "Return the strings from the file that are likely AES ECB encoded."
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          for index from 0
          while line
          when (likely-aes-ecb-string line)
            do (return line))))

(defun likely-aes-ecb-string (string)
  "Is this string likely AES ECB? This uses the property of ECB where repeated blocks of input
  are encrypted identically. This probably won't work on short inputs."
  (loop with table = (make-hash-table :test 'equalp)
        for index from 0 below (- (length string) 32) by 32
        do (let ((key (subseq string index (+ index 32))))
             (if (gethash key table)
                 (return-from likely-aes-ecb-string t)
                 (setf (gethash key table) t))))
  nil)

(defun pad-vect-pkcs7 (vect new-size)
  "Pad the vector according to PKCS7, to the new length. The new size must be less than 256 bytes
  larger than the old."
  (let ((pad-length (- new-size (length vect))))
    (assert (< pad-length 256))
    (loop with padded = (make-byte-vect new-size :initial-element pad-length)
          for index from 0
          for entry across vect
          do (setf (aref padded index) entry)
          finally (return padded))))

(defun unpad-vect-pkcs7 (vect)
  "Unpad the vector according to PKCS7. If the padding and the pad-length differ, throw an error.
  Note, this assumes the vector is padded."
  (if (valid-pkcs7 vect)
      (let ((vect-length (length vect)))
        (trim-byte-vect vect (- vect-length (aref vect (1- vect-length)) 1)))
      (error "Invalid padding in vector.")))

(defun valid-pkcs7 (vect)
  "Valid padding consists of the pad length in each byte at the end of the vector for that number
  of bytes. Eg #(1 2 3 4 4 4 4) has a pad-length of 4."
  (loop with vect-length = (length vect)
        with final-index = (1- vect-length)
        with pad-length = (aref vect final-index)
        for pad-index from final-index downto (- vect-length pad-length)
        for pad-content = (aref vect pad-index)
        when (/= pad-length pad-content)
          do (return nil)
        finally (return t)))

(defun encrypt-string-file-to-aes-cbc-to-b64 (key init path)
  "Encrypt the string as AES in CBC mode then return it as b64. Note, the key determines the block
  length."
  (let ((key-vect (string-to-byte-vect key))
        (input (read-string-file-to-byte-vect path)))
    (byte-vect-to-string (encrypt-aes-cbc input key-vect init))))

(defun encrypt-aes-cbc (in-vect key-vect init-vect)
  "Encrypt the AES mode CBC in-vect byte vector using the key-vect as the key and the init-vect
  as the initialization vector. Note, the key determines the block length"
  (let* ((key-length (length key-vect))
         (in-vect-length (length in-vect))
         (extra-bytes-in-final-block (mod in-vect-length key-length))
         (final-block-length (- key-length extra-bytes-in-final-block)))
    (assert (= key-length (length init-vect)))
    (loop with output = (make-byte-vect (+ in-vect-length (if (zerop extra-bytes-in-final-block)
                                                              0 final-block-length)))
          with previous = init-vect
          for index from 0 below (- in-vect-length extra-bytes-in-final-block) by key-length
          do (let* ((xored (byte-vect-xor previous (copy-byte-vect-from
                                                    in-vect index key-length)))
                    (temp (encrypt-aes-ecb xored key-vect)))
               (setf previous temp)
               (copy-byte-vect-into temp output index))
          finally (unless (zerop extra-bytes-in-final-block)
                     (let* ((xored (byte-vect-xor previous (copy-byte-vect-from in-vect
                                                                               (- in-vect-length
                                                                                  final-block-length)
                                                                               final-block-length)))
                            (temp (encrypt-aes-ecb xored key-vect)))
                       (copy-byte-vect-into temp output index)))
                  (return output))))

(defun decrypt-aes-cbc (in-vect key-vect init-vect)
  "Decrypt the AES mode CBC in-vect byte vector using the key-vect as the key and the init-vect
  as the initialization vector. Note, the key determines the block length"
  (let ((key-length (length key-vect))
        (in-vect-length (length in-vect)))
    (assert (zerop (mod in-vect-length key-length)))
    (loop with output = (make-byte-vect in-vect-length)
          with current = (copy-byte-vect-from in-vect (- in-vect-length key-length) key-length)
          for index from (- in-vect-length key-length) downto key-length by key-length
          for previous = (copy-byte-vect-from in-vect (- index key-length) key-length)
          do (let ((temp (decrypt-aes-ecb current key-vect)))
               (copy-byte-vect-into (byte-vect-xor temp previous) output index)
               (setf current previous))
          finally (let ((temp (decrypt-aes-ecb current key-vect)))
                    (copy-byte-vect-into (byte-vect-xor temp init-vect) output index)
                    (return (trim-byte-vect output))))))

(defun decrypt-b64-file-aes-cbc (key init path)
  "Decrypt the b64 file using AES in CBC mode. Note, the key determines the block length."
  (let* ((key-vect (string-to-byte-vect key))
         (input (read-b64-file-to-byte-vect path)))
    (byte-vect-to-string (decrypt-aes-cbc input key-vect init))))

(defun encrypt-aes-ecb-padded (in-vect key-vect)
  (let* ((in-length (length in-vect))
         (key-length (length key-vect))
         (padded (if (plusp (mod in-length key-length))
                     (pad-vect-pkcs7 in-vect (next-pad-size in-length key-length))
                     in-vect)))
    (encrypt-aes-ecb padded key-vect)))

(defun next-pad-size (length key-length)
  "Return the next possible pad size given the key-length, larger than the length."
  (let ((remainder (mod length key-length)))
    (+ key-length (- length remainder))))

(defun decrypt-aes-ecb-padded (encrypted key)
  "Decrypt the AES ECB crypt-text and remove the padding if present."
  (unpad-vect-pkcs7 (decrypt-aes-ecb encrypted key)))

(defun pad-vect-pkcs7-to-block (vect block-size)
  "Pad the vector to the block, always padding even if the vector ends at a block end."
  (pad-vect-pkcs7 vect (round-up-to-mod (length vect) block-size)))

(defun encrypt-aes-ctr (plaintext key nonce)
  (decrypt-aes-ctr plaintext key nonce))

(defun decrypt-aes-ctr (crypttext key nonce)
  (labels ((increment-counter (counter &optional index)
			       (if (< (aref counter index) 255)
			           (incf (aref counter index))
				         (progn (setf (aref counter index) 0)
				                (increment-counter counter (1+ index))))))
    (let ((block-size (length key))
          (length (length crypttext)))
      (assert (evenp block-size))
      (loop with counter = (make-byte-vect block-size)
            with plaintext = (make-byte-vect length)
              initially (copy-byte-vect-into nonce counter 0)
            for index from 0 below length by block-size
            do (let* ((crypt-counter (encrypt-aes-ecb counter key))
                      (xored (byte-vect-xor crypt-counter
                                            (copy-byte-vect-from crypttext index block-size))))
                 (copy-byte-vect-into xored plaintext index))
               (increment-counter counter (/ block-size 2))
            finally (return plaintext)))))

(defun attempt-decode-xor-repeating-key-vect-list (vect-list)
  "Attack the vector-list, assuming it is an english string encoded with a repeating key, with the
  list being blocks of encoded text, each having been encoded with an identical key."
  (loop for chunk in (transpose-byte-vects vect-list)
        collecting (multiple-value-bind (vect key)
                       (attempt-decode-xor-against-one-repeated-char chunk)
                     (declare (ignore vect))
                     key)
          into keys
        finally (return (list-to-byte-vect keys))))
