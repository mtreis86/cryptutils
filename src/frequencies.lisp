(defpackage cryptutils/frequencies
  (:use :cl)
  (:import-from :cryptutils/utils
                :alist-key-union
                :longer-list
                :shorter-list)
  (:export 
   :*book-list*
   :*english-char-freqs-no-spaces*
   :*english-char-freqs*
   :*english-code-freqs-no-spaces*
   :*english-code-freqs*
   :*lowercase-chars*
   :*uppercase-chars*
   :*digit-chars*
   :*rare-punctuation-chars*
   :*common-punctuation-chars*
   :*whitespace-chars*
   :*sub-word-chars*
   :*lowercase-codes*
   :*uppercase-codes*
   :*digit-codes*
   :*rare-punctuation-codes*
   :*common-punctuation-codes*
   :*whitespace-codes*
   :*sub-word-codes*
   :select-most-likely-english
   :more-likely-english
   :code-freq
   :uncase-code-freqs
   :code-downcase
   :code-upcase
   :char-freqs-to-code-freqs
   :freq-difference
   :lookup-freq
   :word
   :map-word-table
   :dump-grams
   :update-grams
   :convert-freqs-to-counts
   :convert-counts-to-freqs
   :lookup-gram
   :lookup-digram
   :lookup-trigram
   :lookup-word
   :map-nested-hash
   :initialize-grams))

(in-package :cryptutils/frequencies)

;;;; Character and char-code frequency related functions.

;;; character frequency is an alist of (char . freq) where freq is a float of percentage 100% = 1
;;; quick test, total freqs in the table should add up to 1



(defvar *book-list*
  '(#P"~/common-lisp/cryptutils/books/paradiselost.txt"))

(defvar *english-char-freqs-no-spaces*
  '((#\A . 0.08200) (#\B . 0.01500) (#\C . 0.02800) (#\D . 0.04300) (#\E . 0.13000) (#\F . 0.02200)
    (#\G . 0.02000) (#\H . 0.06100) (#\I . 0.07000) (#\J . 0.00150) (#\K . 0.00770) (#\L . 0.04000)
    (#\M . 0.02400) (#\N . 0.06700) (#\O . 0.07500) (#\P . 0.01900) (#\Q . 0.00095) (#\R . 0.06000)
    (#\S . 0.06300) (#\T . 0.09100) (#\U . 0.02800) (#\V . 0.00980) (#\W . 0.02400) (#\X . 0.00150)
    (#\Y . 0.02000) (#\Z . 0.00074))
  "An alist of English character frequencies, using the ascii code as the char lookup.
  Doesn't include spaces or other whitespace, nor punctuation. Found on some website.
  At some point I will generate these myself.")

(defvar *english-char-freqs*
  '((#\A . 0.0651738) (#\B . 0.0124248) (#\C . 0.0217339) (#\D . 0.0349835) (#\E . 0.1041442)
    (#\F . 0.0197881) (#\G . 0.0158610) (#\H . 0.0492888) (#\I . 0.0558094) (#\J . 0.0009033)
    (#\K . 0.0050529) (#\L . 0.0331490) (#\M . 0.0202124) (#\N . 0.0564513) (#\O . 0.0596302)
    (#\P . 0.0137645) (#\Q . 0.0008606) (#\R . 0.0497563) (#\S . 0.0515760) (#\T . 0.0729357)
    (#\U . 0.0225134) (#\V . 0.0082903) (#\W . 0.0171272) (#\X . 0.0013692) (#\Y . 0.0145984)
    (#\Z . 0.0007836) (#\SPACE . 0.1918182))
   "An alist of English character frequencies, using the ascii code as the char lookup.
   Does include spaces, but no other whitespace, nor punctuation. Found on some website.
   At some point I will generate these myself.")

(defvar *english-code-freqs-no-spaces*
  '((097 . 0.08200) (098 . 0.01500) (099 . 0.02800) (100 . 0.04300) (101 . 0.13000) (102 . 0.02200)
    (103 . 0.02000) (104 . 0.06100) (105 . 0.07000) (106 . 0.00150) (107 . 0.00770) (108 . 0.04000)
    (109 . 0.02400) (110 . 0.06700) (111 . 0.07500) (112 . 0.01900) (113 . 0.00095) (114 . 0.06000)
    (115 . 0.06300) (116 . 0.09100) (117 . 0.02800) (118 . 0.00980) (119 . 0.02400) (120 . 0.00150)
    (121 . 0.02000) (122 . 0.00740))
  "An alist of English char-code frequencies, using the ascii code as the char lookup.
  Doesn't include spaces or other whitespace, nor punctuation. Found on some website.
  At some point I will generate these myself.")

(defvar *english-code-freqs*
  '((097 . 0.0651738) (098 . 0.0124248) (099 . 0.0217339) (100 . 0.0349835) (101 . 0.1041442)
    (102 . 0.0197881) (103 . 0.0158610) (104 . 0.0492888) (105 . 0.0558094) (106 . 0.0009033)
    (107 . 0.0050529) (108 . 0.0331490) (109 . 0.0202124) (110 . 0.0564513) (111 . 0.0596302)
    (112 . 0.0137645) (113 . 0.0008606) (114 . 0.0497563) (115 . 0.0515760) (116 . 0.0729357)
    (117 . 0.0225134) (118 . 0.0082903) (119 . 0.0171272) (120 . 0.0013692) (121 . 0.0145984)
    (122 . 0.0007836) (032 . 0.1918182))
   "An alist of English char-code frequencies, using the ascii code as the char lookup.
   Does include spaces, but no other whitespace, nor punctuation. Found on some website.
   At some point I will generate these myself.")

(defvar *lowercase-chars*
  (loop for code from (char-code #\a) to (char-code #\z) collecting (code-char code)))

(defvar *uppercase-chars*
  (loop for code from (char-code #\A) to (char-code #\Z) collecting (code-char code)))

(defvar *digit-chars*
  (loop for code from (char-code #\0) to (char-code #\9) collecting (code-char code)))

(defvar *rare-punctuation-chars*
  '(#\@ #\# #\* #\+ #\- #\= #\\ #\/ #\{ #\} #\| #\^ #\` #\~ #\< #\>))

(defvar *common-punctuation-chars*
  '(#\, #\. #\? #\; #\: #\' #\" #\! #\$ #\% #\& #\( #\)))

(defvar *whitespace-chars*
  '(#\SPACE #\RETURN #\NEWLINE #\LINEFEED #\TAB #\NUL))

(defvar *sub-word-chars*
  (append *uppercase-chars* *lowercase-chars* (list #\')))

(defvar *lowercase-codes*
  (loop for char in *lowercase-chars* collecting (char-code char)))

(defvar *uppercase-codes*
  (loop for char in *uppercase-chars* collecting (char-code char)))

(defvar *digit-codes*
  (loop for char in *digit-chars* collecting (char-code char)))

(defvar *rare-punctuation-codes*
  (loop for char in *rare-punctuation-chars* collecting (char-code char)))

(defvar *common-punctuation-codes*
  (loop for char in *common-punctuation-chars* collecting (char-code char)))

(defvar *whitespace-codes*
  (loop for char in *whitespace-chars* collecting (char-code char)))

(defvar *sub-word-codes*
  (loop for char in *sub-word-chars* collecting (char-code char)))

(defun select-most-likely-english (vect-list)
  "Which of the vects in the list is most likely english?"
  (first (sort vect-list #'more-likely-english :key #'first)))

(defun more-likely-english (vect1 vect2)
  "Is the first vect more likely english than the second?"
  (let ((freq1 (uncase-code-freqs (code-freq vect1)))
        (freq2 (uncase-code-freqs (code-freq vect2))))
    (< (freq-difference freq1 *english-code-freqs*)
       (freq-difference freq2 *english-code-freqs*))))

(defun code-freq (vect)
  "Return an alist of char-code frequencies by counting the instances of each character then
  dividing that by the total length of the vector. Case is not ignored so this will collect
  A and a."
  (loop with freqs = '()
        for entry across vect
        do (if (assoc entry freqs)
               (incf (rest (assoc entry freqs)))
               (push (cons entry 1) freqs))
        finally (return (loop for (entry . count) in freqs
                              do (setf (rest (assoc entry freqs))
                                       (float (/ count (length vect))))
                              finally (return freqs)))))

(defun uncase-code-freqs (code-freqs)
  "Convert the code freqiencies to a downcased version."
  (loop with uncased = '()
        for (code . count) in code-freqs
        for downcase = (code-downcase code)
        if (assoc downcase uncased)
          do (incf (rest (assoc downcase uncased)) count)
        else do (push (cons downcase count) uncased)
        finally (return uncased)))

(defun code-downcase (code)
  "Downcase the char-code."
  (let ((lower-case-a-code (char-code #\a))
        (upper-case-a-code (char-code #\A))
        (upper-case-z-code (char-code #\Z)))
    (if (and (>= code upper-case-a-code)
             (<= code upper-case-z-code))
        (+ (- code upper-case-a-code) lower-case-a-code)
        code)))

(defun code-upcase (code)
  "Upcase the char-code."
  (let ((upper-case-a-code (char-code #\A))
        (lower-case-a-code (char-code #\a))
        (lower-case-z-code (char-code #\z)))
    (if (and (>= code lower-case-a-code)
             (<= code lower-case-z-code))
        (+ (- code lower-case-a-code) upper-case-a-code)
        code)))

(defun char-freqs-to-code-freqs (char-freqs)
  "Convert the char-frequencies alist to one of code-frequencies."
  (loop with codes = '()
        for (char . freq) in char-freqs
        do (push (cons (char-code char) freq) codes)
        finally (return codes)))

(defun freq-difference (freq-table-1 freq-table-2)
  "Return the difference of the frequency tables. The higher the number the more different
   the frequencies."
  (declare (type list freq-table-1 freq-table-2))
  (loop with longer-table = (longer-list freq-table-1 freq-table-2)
        with shorter-table = (shorter-list freq-table-1 freq-table-2)
        for code in (alist-key-union longer-table shorter-table)
        for freq1 = (lookup-freq code shorter-table)
        for freq2 = (lookup-freq code longer-table)
        summing (abs (- freq1 freq2)) into diff
        finally (return diff)))

(defun lookup-freq (code table)
  "Lookup the frequency in the table, if it doesn't exist return 0."
  (let ((freq (rest (assoc code table))))
    (if freq freq 0)))

(defclass word ()
  ((count :initform 1 :accessor word-count)
   (freq :initform 0.0 :accessor word-freq)
   (table :initform (make-hash-table :test #'eq) :reader word-table)))

(defun map-word-table (fn table)
  (maphash (lambda (code word)
             (declare (ignore code))
             (map-word-table fn (word-table word))
             (funcall fn word))
           table))

(let ((ungrams (make-hash-table :test #'eq))
      (digrams (make-hash-table :test #'eq))
      (trigrams (make-hash-table :test #'eq))
      (words (make-hash-table :test #'equalp))
      (un-total 0)
      (di-total 0)
      (tri-total 0)
      (word-total 0)
      (total 0)
      (state 'counts))
  (defun dump-grams ()
    (values ungrams digrams trigrams words))
  (defun update-grams (paths)
    (when (eq state 'freqs) (convert-freqs-to-counts))
    (reset-current-word)
    (loop for path in paths
          do (with-open-file (stream path)
               (loop with pprev = 0
                     with prev = 0
                     for char = (read-char stream nil)
                     while char
                     for code = (char-code char)
                     do (if (gethash code ungrams) ; ungrams
                            (incf (gethash code ungrams))
                            (progn (setf (gethash code ungrams) 1)
                                   (incf un-total)))
                        (if (gethash prev digrams) ; digrams
                            (if (gethash code (gethash prev digrams))
                                (incf (gethash code (gethash prev digrams)))
                                (progn (setf (gethash code (gethash prev digrams)) 1)
                                       (incf di-total)))
                            (progn (setf (gethash prev digrams) (make-hash-table :test #'eq)
                                         (gethash code (gethash prev digrams)) 1)
                                   (incf di-total)))
                        (if (gethash pprev trigrams) ; trigrams
                            (if (gethash prev (gethash pprev trigrams))
                                (if (gethash code (gethash prev (gethash pprev trigrams)))
                                    (incf (gethash code (gethash prev (gethash pprev trigrams))))
                                    (progn (setf (gethash code
                                                          (gethash prev
                                                                   (gethash pprev trigrams)))
                                                 1)
                                           (incf tri-total)))
                                (progn (setf (gethash prev (gethash pprev trigrams))
                                             (make-hash-table :test #'eq)
                                             (gethash code (gethash prev (gethash pprev trigrams)))
                                             1)
                                       (incf tri-total)))
                            (progn (setf (gethash pprev trigrams)
                                         (make-hash-table :test #'eq)
                                         (gethash prev (gethash pprev trigrams))
                                         (make-hash-table :test #'eq)
                                         (gethash code (gethash prev (gethash pprev trigrams)))
                                         1)
                                   (incf tri-total)))
                        (if (find prev *sub-word-codes*) ; words
                            (if (find code *sub-word-codes*)
                                (extend-last-word code)
                                (terminate-word))
                            (when (find code *sub-word-codes*)
                              (start-word code)))
                        (incf total)
                        (setf pprev prev
                              prev code))))
    (convert-counts-to-freqs)
    (values un-total di-total tri-total word-total))
  (let ((current-word nil))
    (defun reset-current-word ()
      (setf current-word nil))
    (defun extend-last-word (code)
      (if (not current-word)
          (start-word code)
          (let ((word-exists (gethash code (word-table current-word))))
            (if word-exists
                (setf current-word word-exists)
                (setf (gethash code (word-table current-word)) (make-instance 'word))))))
    (defun terminate-word ()
      (incf (word-count current-word))
      (incf word-total)
      (setf current-word nil))
    (defun start-word (code)
      (let ((new-word (make-instance 'word)))
        (setf (gethash code words) new-word
              current-word new-word))))
  (defun convert-freqs-to-counts ()
    (when (eq state 'freqs)
      (maphash (lambda (code freq)
                 (setf (gethash code ungrams)
                       (round (* freq total))))
               ungrams)
      (maphash (lambda (prev p-table)
                 (maphash (lambda (code freq)
                           (setf (gethash code (gethash prev digrams))
                                 (round (* freq total))))
                          p-table))
               digrams)
      (maphash (lambda (pprev pp-table)
                 (maphash (lambda (prev p-table)
                            (maphash (lambda (code freq)
                                       (setf (gethash code (gethash prev (gethash pprev trigrams)))
                                             (round (* freq total))))
                                     p-table))
                          pp-table))
               trigrams))
    (setf state 'counts))
  (defun convert-counts-to-freqs ()
    (when (eq state 'counts)
      (maphash (lambda (code count)
                 (setf (gethash code ungrams)
                       (float (/ count total))))
               ungrams)
      (maphash (lambda (prev p-table)
                 (maphash (lambda (code count)
                            (setf (gethash code (gethash prev digrams))
                                  (float (/ count total))))
                          p-table))
               digrams)
      (maphash (lambda (pprev pp-table)
                 (maphash (lambda (prev p-table)
                            (maphash (lambda (code count)
                                       (setf (gethash code (gethash prev (gethash pprev trigrams)))
                                             (float (/ count total))))
                                     p-table))
                          pp-table))
               trigrams)
      (map-word-table (lambda (word)
                        (setf (word-freq word) (float (/ (word-count word) word-total))))
                      words))
    (setf state 'freqs))
  (defmethod lookup-gram ((code number))
    (gethash code ungrams))
  (defmethod lookup-gram ((char character))
    (gethash (char-code char) ungrams))
  (defmethod lookup-gram ((list list))
    (cond ((every #'numberp list)
           (ecase (length list)
             (1 (gethash (first list) ungrams))
             (2 (lookup-digram (first list) (second list)))
             (3 (lookup-trigram (first list) (second list) (third list)))))
          ((every #'characterp list)
           (ecase (length list)
             (1 (gethash (char-code (first list)) ungrams))
             (2 (lookup-digram (char-code (first list))
                               (char-code (second list))))
             (3 (lookup-trigram (char-code (first list))
                                (char-code (second list))
                                (char-code (third list))))))
          (t (error "List contains more than just codes or characters: ~A" list))))
  (defmethod lookup-gram ((string string))
    (ecase (length string)
      (1 (gethash (char-code (char string  0)) ungrams))
      (2 (gethash (char-code (char string 1))
                  (gethash (char-code (char string 0))
                           digrams)))
      (3 (gethash (char-code (char string 2))
                  (gethash (char-code (char string 1))
                           (gethash (char-code (char string 0))
                                    trigrams))))))
  (defun lookup-digram (first second)
    (let ((first-gram (gethash first digrams)))
      (when first-gram (gethash second first-gram))))
  (defun lookup-trigram (first second third)
    (let ((first-gram (gethash first trigrams)))
      (when first-gram
        (let ((second-gram (gethash second first-gram)))
          (when second-gram (gethash third second-gram))))))
  (defun lookup-word (code-list)
    (loop with prev = words
          with last-index = (1- (length code-list))
          for code in code-list
          for index from 0
          do (let ((word (gethash code prev)))
               (if word
                   (if (< index last-index)
                       (setf prev (word-table word))
                       (return (values (word-freq word) t)))
                   (return nil))))))

(defun map-nested-hash (fn table)
  "Works like maphash but on nested hash tables where the value may be another table. Applies the
  fn only when the value isn't a table, and takes a (lambda (key value) ...) as the fn to apply."
  (maphash (lambda (key value)
             (if (hash-table-p value)
                 (map-nested-hash fn value)
                 (funcall fn key value)))
    table))

(defun initialize-grams ()
  (update-grams *book-list*))












