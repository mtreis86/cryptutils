(defpackage cryptutils/io
  (:use :cl)
  (:import-from :bit-foolery
                :shift-high
                :shift-low)
  (:import-from :cryptutils/math
                :min-frame-width)
  (:import-from :cryptutils/utils
                :sequencep)
  (:export
   :*print-hex-case*
   :hex-char-p
   :hex-code-p
   :hex-char
   :hex-code
   :char-hex
   :write-hex
   :print-hex
   :hex-string-p
   :hex-string
   :parse-hex
   :hex-string-to-b64-string
   :hex-string-to-byte-vect

   :b64-char-p
   :b64-code-p
   :b64-char
   :b64-code
   :b64-pad
   :char-b64
   :b64-string-p
   :b64-string
   :parse-b64
   :write-b64
   :print-b64
   :read-b64-file-to-byte-vect
   :b64-string-to-byte-vect
   :byte-vect-to-b64-string
   :read-b64
   :b64-chars-to-byte
   :b64-codes-to-byte

   :print-bin
   :write-bin
   :bin-char

   :make-byte-vect
   :print-vect-as-string
   :print-byte-vect-as-hex
   :string-to-byte-vect
   :byte-vect-to-string
   :byte-vect-list-to-byte-vect
   :trim-byte-vect
   :string-list-to-byte-vect
   :copy-byte-vect-from
   :copy-byte-vect-into
   :read-string-file-to-byte-vect
   :make-random-byte-vect
   :byte-vector-ranges=
   :copy-byte-vect-range
   :copy-byte-vect-from-index-back
   :swap-blacklist-chars-for-escape
   :copy-byte-vect
   :transpose-chunks
   :transpose-byte-vects))

(in-package :cryptutils/io)


;;;hex
(defvar *print-hex-case* :lower)

(defun hex-char-p (char)
  "A hex char is one of 0-9, A-F, or a-f."
  (let ((code (char-code char))
        (lower-a-code (char-code #\a))
        (lower-f-code (char-code #\f))
        (upper-a-code (char-code #\A))
        (upper-f-code (char-code #\F))
        (0-code (char-code #\0))
        (9-code (char-code #\9)))
    (or (and (>= code lower-a-code)
             (<= code lower-f-code))
        (and (>= code upper-a-code)
             (<= code upper-f-code))
        (and (>= code 0-code)
             (<= code 9-code)))))

(defun hex-code-p (code)
  "A hex-code is an integer between 0 and 15 inclusive."
  (and (typep code 'integer)
       (>= code 0)
       (<= code 15)))

(deftype hex-char ()
  "A hex-char is one of 0-9, A-F, or a-f."
  `(satisfies hex-char-p))

(deftype hex-code ()
  "A hex-code is an integer between 0 and 15 inclusive."
  `(satisfies hex-code-p))

(defun hex-char (code)
  "Return the hex char for the code where 0->0 and 15->f. If the code is above 9, and case is :upper,
 return an upper-case char, otherwise the default is :lower to return a lower-case char."
  (assert (typep code 'hex-code))
  (code-char (if (<= code 9)
                 (+ code (char-code #\0))
                 (+ code -10 (cond ((eq *print-hex-case* :lower)
                                    (char-code #\a))
                                   ((eq *print-hex-case* :upper)
                                    (char-code #\A))
                                   (t (error "Invalid print-hex-case: ~A" *print-hex-case*)))))))


(defun char-hex (char)
  "Return the 4-bit hex integer represented by the character."
  (assert (typep char 'hex-char))
  (let* ((code (char-code char))
         (lower-a-code (char-code #\a))
         (lower-f-code (char-code #\f))
         (upper-a-code (char-code #\A))
         (upper-f-code (char-code #\F))
         (0-code (char-code #\0))
         (9-code (char-code #\9)))
    (cond ((and (>= code 0-code)
                (<= code 9-code))
           (- code 0-code))
          ((and (>= code lower-a-code)
                (<= code lower-f-code))
           (- code -10 lower-a-code))
       ((and (>= code upper-a-code)
             (<= code upper-f-code))
        (- code -10 upper-a-code)))))

(defun write-hex (number &key stream frame-width)
  "Write the number, in hex, to the place. Typical places will be streams and strings."
  (let ((frame-width (if frame-width frame-width
                         (min-frame-width number 16))))
    (loop for index from (* (1- frame-width) 4) downto 0 by 4
          do (write-char
               (hex-char (ldb (byte 4 index)
                              number))
               stream)))
  number)

(defun print-hex (number &optional (stream *standard-output*) frame-width)
  "Print the number in hexidecimal. Print to the stream if given, otherwise
  return a string."
  (if stream
      (progn
        (fresh-line stream)
        (write-hex number :stream stream :frame-width frame-width))
      (with-output-to-string (string)
        (write-hex number :stream string :frame-width frame-width))))


(defun hex-string-p (string)
  (loop for char across string
        unless (typep char 'hex-char)
          do (return nil)
        finally (return t)))

(deftype hex-string ()
  `(satisfies hex-string-p))

(defun parse-hex (string)
  "Convert the hex string into a number."
  (assert (typep string 'hex-string))
  (loop with output = 0
        for char across string
        for position downfrom (* 4 (1- (length string))) by 4
        do (setf output
                 (dpb (char-hex char)
                      (byte 4 position)
                      output))
        finally (return output)))
(defun hex-string-to-b64-string (hex &optional (stream *standard-output*))
  "Copy and convert the hex string (4-bit) to a b64 string (6-bit)."
  (let ((number (parse-integer hex :radix 16)))
    (print-b64 number :stream stream)))

(defun hex-string-to-byte-vect (string)
  "Copy and convert the hex string into a byte vector. Since the hex string is 4-bit and the
  bytes are 8-bit, use two hex ints per byte."
  (loop with length = (length string)
        with vect = (make-byte-vect (ceiling length 2))
        for index from 0 below length by 2
        for char1 = (char string index)
        for char2 = (char string (1+ index))
        do (setf (aref vect (floor index 2)) (dpb (hex-code char2) (byte 4 0)
                                                  (shift-high (hex-code char1) 8 4)))
        finally (when (oddp length)
                  (setf (aref vect (1+ index))
                        (shift-high (hex-code (char string (1+ index))) 8 4)))
                (return vect)))

(defun hex-code (char)
  "Return the hex-code for the character. Will be a 4-bit integer."
  (let ((a-code (char-code #\a))
        (f-code (char-code #\f))
        (0-code (char-code #\0))
        (9-code (char-code #\9))
        (code (char-code (char-downcase char))))
    (cond ((and (>= code a-code)
                (<= code f-code))
           (- code -10 a-code))
          ((and (>= code 0-code)
                (<= code 9-code))
           (- code 0-code))
          (t (Error "~A is not a hex character." char)))))
;;; b64


(defun b64-char-p (char)
  "A b64 char is one of 0-9, A-Z, a-z. Or one of #\\ #\+ #\=."
  (let ((code (char-code char))
        (lower-a-code (char-code #\a))
        (lower-z-code (char-code #\z))
        (upper-a-code (char-code #\A))
        (upper-Z-code (char-code #\Z))
        (0-code (char-code #\0))
        (9-code (char-code #\9))
        (/-code (char-code #\/))
        (+-code (char-code #\+)))
    (or (and (>= code lower-a-code)
             (<= code lower-z-code))
        (and (>= code upper-a-code)
             (<= code upper-z-code))
        (and (>= code 0-code)
             (<= code 9-code))
        (= code /-code)
        (= code +-code))))

(defun b64-code-p (code)
  "A b64-code is an integer between 0 and 63 inclusive."
  (typep code '(integer 0 63)))

(defun b64-pad-p (char)
  "A b64-pad is the = character."
  (char= char #\=))

(deftype b64-char ()
  "A b64 char is one of 0-9, A-Z, a-z. Or one of #\\ #\+ #\=."
  `(satisfies b64-char-p))

(deftype b64-code ()
  "A b64-code is an integer between 0 and 63 inclusive."
  `(satisfies b64-code-p))

(deftype b64-pad ()
  "A b64-pad is the = character."
  `(satisfies b64-pad-p))

(defun b64-char (b64-code)
  "Return the b64 char for the 6-bit code."
  (assert (typep b64-code 'b64-code))
  (let ((code (cond ((<= b64-code 25)
                     (+ b64-code (char-code #\A)))
                    ((<= b64-code 51)
                     (+ -26 b64-code (char-code #\a)))
                    ((<= b64-code 61)
                     (+ -52 b64-code (char-code #\0)))
                    ((= b64-code 62)
                     (char-code #\+))
                    ((= b64-code 63)
                     (char-code #\/))
                    (t (error "Not a b64-code")))))
    (code-char code)))

(defun char-b64 (char)
 "Return the 6-bit b64 code represented by the b64 character."
 (assert (typep char 'b64-char))
 (let ((upper-case-a-code (char-code #\A))
       (upper-case-z-code (char-code #\Z))
       (lower-case-a-code (char-code #\a))
       (lower-case-z-code (char-code #\z))
       (zero-code (char-code #\0))
       (nine-code (char-code #\9))
       (code (char-code char)))
   (cond ((and (>= code upper-case-a-code)
               (<= code upper-case-z-code))
          (- code upper-case-a-code))
         ((and (>= code lower-case-a-code)
               (<= code lower-case-z-code))
          (- code -26 lower-case-a-code))
         ((and (>= code zero-code)
               (<= code nine-code))
          (- code -52 zero-code))
         ((= code (char-code #\+))
          62)
         ((= code (char-code #\/))
          63))))

(defun b64-code (char)
  "Return the b64 code for the b64 character. Will be a 6-bit integer or nil in the case of =."
  (let ((lower-case-a-code (char-code #\a))
        (lower-case-z-code (char-code #\z))
        (upper-case-a-code (char-code #\A))
        (upper-case-z-code (char-code #\Z))
        (zero-code (char-code #\0))
        (nine-code (char-code #\9))
        (code (char-code char)))
    (cond ((and (>= code lower-case-a-code)
                (<= code lower-case-z-code))
           (- code -26 lower-case-a-code))
          ((and (>= code upper-case-a-code)
                (<= code upper-case-z-code))
           (- code upper-case-a-code))
          ((and (>= code zero-code)
                (<= code nine-code))
           (- code -52 zero-code))
          ((= code (char-code #\+))
           62)
          ((= code (char-code #\/))
           63)
          ((= code (char-code #\=))
           nil)
          (t (error "~A is not a b64 character" char)))))

(defun b64-string-p (string)
 (loop with length = (length string)
       for index from 0
       for char across string
       unless (or (typep char 'b64-char)
                  (and (> index (- length 3))
                       (typep char 'b64-pad)))
         do (return nil)
       finally (return t)))

(deftype b64-string ()
  `(satisfies b64-string-p))

(defun parse-b64 (string)
  "Convert the b64 string into a number."
  (assert (typep string 'b64-string))
  (loop with output = 0
        with pads = 0
        for char across string
        for position downfrom (* 6 (1- (length string))) by 6
        if (typep char 'b64-char)
          do (setf output
                   (dpb (char-b64 char)
                        (byte 6 position)
                        output))
        else do (incf pads)
        finally (return (ash output (* pads -8)))))

(defun print-b64 (number &key frame-width (stream *standard-output*))
  "Print the number in b64."
  (let ((frame-width (cond (frame-width frame-width)
                           ((or (zerop number) (= number 1)) 1)
                           (t (ceiling (log number 64))))))
    (if stream
        (with-open-stream (out (if (streamp stream) stream *standard-output*))
          (fresh-line stream)
          (write-b64 number frame-width out)
          number)
        (with-output-to-string (string)
          (write-b64 number frame-width string)))))

(defun write-b64 (number frame-width place)
  "Write the number, in b64, to the place. Typical places will be streams and strings."
  (loop for index from (1- frame-width) downto 0
        do (write-char (b64-char (ldb (byte 6 (* index 6)) number)) place)))

(defun read-b64-file-to-byte-vect (path)
  "Open the file and read it into a byte-vector, decoding the file from b64 to b256."
  (with-open-file (stream path)
    (loop with file-length = (file-length stream)
          with byte-vect = (make-byte-vect file-length)
          for index from 0 to (* file-length 3/4) by 3
          for char0 = (read-b64 stream nil)
          for char1 = (read-b64 stream nil)
          for char2 = (read-b64 stream nil)
          for char3 = (read-b64 stream nil)
          when (and char0 char1)
            do (setf (aref byte-vect index) (b64-chars-to-byte char0 char1 0))
          when (and char1 char2)
            do (setf (aref byte-vect (+ 1 index)) (b64-chars-to-byte char1 char2 1))
          when (and char2 char3)
            do (setf (aref byte-vect (+ 2 index)) (b64-chars-to-byte char2 char3 2))
          finally (return (trim-byte-vect byte-vect)))))

(defun b64-string-to-byte-vect (string)
  "Convert the b64 string to a byte vect."
  (loop with length = (length (string-right-trim '(#\=) string))
        with final-block = (mod length 4)
        with vect-length = (if (= final-block 0)
                               (* length 3/4)
                               (ceiling (* length 3/4)))
        with byte-vect = (make-byte-vect vect-length)
        ;initially (format t "~&~A ~A ~A~%" length final-block byte-vect)
        for byte-vect-index from 0 by 3
        for string-index from 0 below (- length final-block) by 4
        for char0 = (char string string-index)
        for char1 = (char string (+ 1 string-index))
        for char2 = (char string (+ 2 string-index))
        for char3 = (char string (+ 3 string-index))
        do ;(format t "~A ~A~%" string-index byte-vect-index)
           (setf (aref byte-vect byte-vect-index) (b64-chars-to-byte char0 char1 0)
                 (aref byte-vect (+ 1 byte-vect-index)) (b64-chars-to-byte char1 char2 1)
                 (aref byte-vect (+ 2 byte-vect-index)) (b64-chars-to-byte char2 char3 2))
        finally ;(format t "~A ~A~%" string-index byte-vect-index)
           (unless (zerop final-block)
             (let ((char0 (char string string-index))
                   (char1 (if (or (= final-block 2) (= final-block 3))
                              (char string (+ string-index 1)) #\A))
                   (char2 (if (= final-block 3) (char string (+ string-index 2)) #\A))
                   (char3 #\A))
               (setf (aref byte-vect byte-vect-index) (b64-chars-to-byte char0 char1 0))
               (when (or (= final-block 2)
                         (= final-block 3))
                 (setf (aref byte-vect (+ 1 byte-vect-index)) (b64-chars-to-byte char1 char2 1)))
               (when (= final-block 3)
                 (aref byte-vect (+ 2 byte-vect-index)) (b64-chars-to-byte char2 char3 2))))
           (return byte-vect)))

(defun byte-vect-to-b64-string (vect)
  "Copy / convert the byte vector to a b64 string. This may add a padding character."
  (with-output-to-string (string)
    (with-open-stream (out string)
      (loop with length = (length vect)
            with final-block = (mod length 3)
            for index from 0 below (- length final-block) by 3
            for slot0 = (aref vect index)
            for slot1 = (aref vect (+ 1 index))
            for slot2 = (aref vect (+ 2 index))
            do (princ (print-b64 (ldb (byte 6 2) slot0)
                                 :stream nil) out)
               (princ (print-b64 (dpb (ldb (byte 2 0) slot0) (byte 2 4)
                                      (shift-low slot1 6 4))
                                 :stream nil) out)
               (princ (print-b64 (dpb (ldb (byte 2 6) slot2) (byte 2 0)
                                      (shift-high slot1 6 2))
                                 :stream nil) out)
               (princ (print-b64 (ldb (byte 6 0) slot2)
                                 :stream nil) out)
            finally (unless (zerop final-block)
                      (let ((slot0 (aref vect index)))
                        (princ (print-b64 (ldb (byte 6 2) slot0)
                                          :stream nil) out)
                        (if (= final-block 1)
                            (princ "=" out)
                            (let ((slot1 (aref vect (+ 1 index))))
                              (princ (print-b64 (dpb (ldb (byte 2 0) slot0) (byte 2 4)
                                                     (shift-low slot1 6 4))
                                                :stream nil) out)
                              (princ "==" out)))))))))

(defun read-b64 (stream &optional eof-value)
  "Read one b64 character off the stream. Will ignore newline return and linefeed. Otherwise
  works similar to read-char."
  (let ((b64 (read-char stream nil eof-value)))
    (if (and b64 (or (char= b64 #\Newline)
                     (char= b64 #\Return)
                     (char= b64 #\Linefeed)))
        (read-b64 stream eof-value)
        b64)))

(defun b64-chars-to-byte (b64-char-1 b64-char-2 offset)
  "Convert the 6-bit b64 chars to an 8-bit byte, using the offset. Offset of 0 means the char1 is
  the highest bits of the result, otherwise the char1 is shifted over 2*offset bits and char2 uses
  more of the space."
  (let ((code-1 (b64-code b64-char-1))
        (code-2 (b64-code b64-char-2)))
    (b64-codes-to-byte code-1 code-2 offset)))

(defun b64-codes-to-byte (code1 code2 offset)
  "Convert the b64-codes to an 8-bit byte, using the offset to shift code2 into the code1 space."
  (let* ((length-2 (* 2 (1+ offset)))
         (length-1 (- 8 length-2))
         (shifted-1 (shift-high code1 8 length-2))
         (shifted-2 (when code2
                      (shift-low code2 length-2 (- length-1 2)))))
    (if code2
        (dpb shifted-2 (byte length-2 0) shifted-1)
        shifted-1)))


;;; binary
(defun print-bin (number &key frame-width (stream *standard-output*))
  "Print the number in binary, if stream is nil return it."
  (let ((frame-width (cond (frame-width frame-width)
                           ((or (zerop number) (= number 1)) 1)
                           (t (ceiling (log number 2))))))
    (if stream
        (progn (with-open-stream (out stream)
                 (fresh-line out)
                 (write-bin number frame-width out))
               number)
        (with-output-to-string (string)
          (write-bin number frame-width string)))))

(defun write-bin (number frame-width place)
  "Write the number, in binary, to the place. Typical places will be streams and strings."
  (loop for index from (1- frame-width) downto 0
      do (write-char (bin-char (ldb (byte 1 index) number)) place)))

(defun bin-char (code)
  "Return the appropriate binary character."
  (assert (or (typep code 'fixnum)
              (typep code 'bit)))
  (assert (or (= code 0) (= code 1)))
  (if (= code 0)
      #\0
      #\1))

;;; byte-vectors
(defun make-byte-vect (length &key (initial-element 0))
  "Make a byte vector."
  (make-array length :element-type '(unsigned-byte 8) :adjustable nil :fill-pointer nil
              :initial-element initial-element))

(defun print-vect-as-string (vect &optional (stream *standard-output*))
  "Print the byte vector as if it was a string, converting the bytes to characters."
  (if stream
      (with-open-stream (out stream)
        (loop for code across vect
              do (write-char (code-char code) stream)
              finally (terpri stream)
                      (return vect)))
      (byte-vect-to-string vect)))

(defun list-to-byte-vect (list)
  "Convert the list of b256 digits to a byte-vector."
  (loop with vect = (make-byte-vect (length list))
        for item in list
        for index from 0
        do (setf (aref vect index) item)
        finally (return vect)))


(defun print-byte-vect-as-hex (vect &optional (stream *standard-output*))
  "Print the byte vector as if it was hex, since the bytes are 8-bit and the hex is 4-bit,
  each byte will print as two hex digits."
  (if stream
      (with-open-stream (out (if (streamp stream) stream *standard-output*))
        (fresh-line out)
        (loop for slot across vect
              do (write-char (hex-char (ldb (byte 4 4) slot)) out)
                 (write-char (hex-char (ldb (byte 4 0) slot)) out))
        (print-byte-vect-as-hex vect nil))
      (with-output-to-string (stream)
        (loop for slot across vect
              do (write-char (hex-char (ldb (byte 4 4) slot)) stream)
                 (write-char (hex-char (ldb (byte 4 0) slot)) stream)))))

(defun byte-vect-to-string (vect)
  "Convert the byte vector to a string of characters."
  (with-output-to-string (string)
    (loop for code across vect
          do (write-char (code-char code) string))))

(defun string-to-byte-vect (string)
  "Convert the string of chars to a byte-vector of codes."
  (loop with length = (length string)
        with vect = (make-byte-vect length)
        for index from 0 below length
        do (setf (aref vect index) (char-code (char string index)))
        finally (return vect)))

(defun byte-vect-list-to-byte-vect (list)
  "Coerce the list of vectors to be one big vector maintaining positions in the list."
  (let ((length (reduce (lambda (x y) (+ (if (sequencep x) (length x) x)
                                         (if (sequencep y) (length y) y)))
                        list)))
    (loop with output = (make-byte-vect length)
          with index = 0
          for vect in list
          do (loop for entry across vect
                   do (setf (aref output index) entry)
                      (incf index))
          finally (return output))))

(defun trim-byte-vect (byte-vect &optional from-end)
 "Trim the zeros from the tail of the vector."
 (loop with last-zero-pos = (if from-end from-end
                                (loop for index from (1- (length byte-vect)) downto 0
                                      while (zerop (aref byte-vect index))
                                      finally (return index)))
       with new-vect = (make-byte-vect (1+ last-zero-pos))
       for index from 0 to last-zero-pos
       do (setf (aref new-vect index)
                (aref byte-vect index))
       finally (return new-vect)))

(defun string-list-to-byte-vect (list)
  "Coerce the list of strings to be one big byte-vect maintaining positions in the list."
  (let ((length (reduce (lambda (x y) (+ (if (sequencep x) (length x) x)
                                         (if (sequencep y) (length y) y)))
                        list)))
    (loop with output = (make-byte-vect length)
          with index = 0
          for string in list
          do (loop for entry across string
                   do (setf (aref output index) (char-code entry))
                      (incf index))
          finally (return output))))

(defun copy-byte-vect-from (input start length)
  "Copy the contents of the byte vect input, from the offset, of length, to a new byte vect."
  (let ((out (make-byte-vect length)))
    (loop for in-index from start below (min (length input) (+ start length))
          for out-index from 0
          do (setf (aref out out-index) (aref input in-index)))
    out))

(defun copy-byte-vect-into (inject into index)
  "Copy the contents of inject, into the byte vect into, starting at the index."
 (loop with in-length = (length inject)
       for in-index from 0 below in-length
       for out-index from index below (min (length into) (+ index in-length))
       do (setf (aref into out-index) (aref inject in-index))))

(defun read-string-file-to-byte-vect (path)
  "Read the file as a string, copying it into a byte-vector. Ignores new-lines, returns,
  and linefeeds."
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          while line
          collecting line into result
          finally (string-list-to-byte-vect result))))

(defun make-random-byte-vect (length)
  "Return a byte vector of the given length filled with random bytes."
  (loop with vect = (make-byte-vect length)
        for index from 0 below length
        do (setf (aref vect index) (random 256))
        finally (return vect)))

(defun byte-vector-ranges= (vector1 start1 end1 vector2 start2 end2)
  "Are the two byte vectors = within the given ranges? Note, this only looks at the shorter range of
  the two ranges, anything beyond that is ignored."
  (assert (and (< end1 (length vector1))
               (< end2 (length vector2))))
  (loop for index1 from start1 to end1
        for index2 from start2 to end2
        when (/= (aref vector1 index1) (aref vector2 index2))
          do (return nil)
        finally (return t)))

(defun copy-byte-vect-range (inject inject-from inject-to target target-from target-to)
  (loop for inject-index from inject-from to inject-to
        for target-index from target-from to target-to
        do (setf (aref target target-index) (aref inject inject-index))))

(defun copy-byte-vect-from-index-back (from from-index length into into-index)
  "Copy the byte vect into the other, from the indexes backwards."
  (loop for f-index from from-index downto (max 0 (- from-index length))
        for i-index from into-index downto (max 0 (- into-index length))
        do (setf (aref into i-index) (aref from f-index)))
  into)

(defun swap-blacklist-chars-for-escape (string black-list escape)
  (loop with new-string = (make-string (length string))
        for index from 0 below (length string)
        if (find (char string index) black-list)
          do (setf (aref new-string index) escape)
        else do (setf (aref new-string index) (aref string index))
        finally (return new-string)))

(defun copy-byte-vect (vect-or-array)
  (loop with length = (length vect-or-array)
        with out = (make-byte-vect length)
        for index from 0 below length
        do (setf (aref out index) (aref vect-or-array index))
        finally (return out)))

(defun transpose-chunks (vect keysize)
  "Return a list of chunks of the vector, of the keysize, where the vector is split into keysize
   number of chunks, and transposed.
   Eg keysize 3 vect 'testing123test' => (ttg3s ei1tt sn2e)"
  (multiple-value-bind (length remainder)
      (ceiling (length vect) keysize)
    (loop for key-index from 0 below keysize
          collecting (loop with vect-length = (if (< key-index (+ keysize remainder))
                                                  length
                                                  (1- length))
                           with result = (make-byte-vect vect-length)
                           for index from 0 below vect-length
                           do (setf (aref result index)
                                    (aref vect (+ (* index keysize) key-index)))
                           finally (return result)))))

(defun transpose-byte-vects (vect-list)
  "Return a list of chunks of the vectors where the nth character of each vector is moved to the
  nth chunk of the result."
  (let (result)
    (labels ((build-result (list index) ; build a list of empty vects to transpose into
               (push (make-byte-vect (length list)) result)
               (let (next-list)
                 (dolist (vect list)
                   (when (> (length vect) (1+ index))
                     (push vect next-list)))
                 (when next-list
                   (build-result (nreverse next-list) (1+ index)))))
             (transpose (list index)
                        (let (next-list)
                          (loop for list-index from 0
                                for vect in list
                                do (setf (aref (nth index result) list-index)
                                         (aref vect index))
                                when (> (length vect) (1+ index))
                                  do (push vect next-list))
                          (when next-list (transpose (nreverse next-list) (1+ index))))))
      (build-result vect-list 0)
      (setf result (nreverse result))
      (transpose vect-list 0)
      result)))
      
      
