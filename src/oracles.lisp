;;;; Oracles and their attacks.

(defpackage cryptutils/oracles
  (:use :cl)
  (:import-from :cryptutils/io
                :b64-code
                :make-random-byte-vect
                :b64-string-to-byte-vect
                :byte-vect-list-to-byte-vect
                :byte-vect-to-string
                :make-byte-vect
                :copy-byte-vect
                :copy-byte-vect-from
                :copy-byte-vect-range
                :string-to-byte-vect
                :swap-blacklist-chars-for-escape)
  (:import-from :cryptutils
                :encrypt-aes-ecb-padded
                :encrypt-aes-cbc
                :decrypt-aes-cbc
                :pad-vect-pkcs7-to-block
                :valid-pkcs7
                :unpad-vect-pkcs7
                :decrypt-aes-ecb-padded
                :pad-vect-pkcs7
                :unpad-vect-pkcs7
                :next-pad-size)
  (:import-from :cryptutils/math
                :round-up-to-mod
                :round-down-to-mod)
  (:import-from :cryptutils/utils
                :split-string
                :strip-chars
                :string-list-to-string
                :unsplit-string-list
                :SUBSTRING-POSITION)










  (:export 
   :aes-oracle
   :determine-aes-oracle-mode
   :determine-aes-ecb-oracle-prepend-secret-length
   :determine-aes-ecb-oracle-data-block-index
   :determine-aes-ecb-oracle-secret-length
   :determine-aes-ecb-oracle-append
   :aes-ecb-oracle-prepend-append
   :dump-aes-ecb-prepend-oracle-static-key
   :dump-aes-ecb-prepend-oracle-appended-secret
   :dump-aes-ecb-prepend-oracle-prepended-secret
   :aes-ecb-oracle
   :dump-aes-ecb-oracle-static-key
   :dump-aes-ecb-oracle-secret
   :determine-aes-oracle-block-size
   :key-val-parse
   :make-user-profile
   :profile-for
   :encode-profile
   :encrypt-profile
   :decrypt-profile
   :force-role=admin
   :encrypt-aes-cbc-bitflip-oracle
   :decrypt-aes-cbc-bitflip-oracle 
   :dump-aes-cbc-bitflip-key
   :test-cbc-bitflip-oracle
   :prep-aes-cbc-injection
   :aes-cbc-force-role=admin
   :make-aes-cbc-cookie
   :decrypt-aes-cbc-cookie
   :verify-aes-cbc-cookie
   :dump-aes-cbc-cookie-key
   :dump-aes-cbc-cookie-init
   :attack-aes-cbc-cookie-oracle))

(in-package :cryptutils/oracles)

;;; Attack ECB oracle that uses a random sequence appended to the end of the user-data to attempt
;;; to obfuscate the data.

(defun aes-oracle (vect &key key mode)
  "Using the key and mode if given, encrypt the vector using AES. If either the key or mode is not
  given, then use random."
  (let* ((prepend-length (+ 5 (random 5)))
         (append-length (+ 5 (random 5)))
         (mode (if mode mode (if (zerop (random 2))
                                 :ecb :cbc)))
         (key (if key key (make-random-byte-vect 16)))
         (padded (byte-vect-list-to-byte-vect (list (make-byte-vect prepend-length)
                                                    vect
                                                    (make-byte-vect append-length)))))
    (cond ((eq mode :ecb)
           (encrypt-aes-ecb-padded padded key))
          ((eq mode :cbc)
           (encrypt-aes-cbc padded key (make-random-byte-vect 16)))
          (t (error "Unknown AES mode: ~A~%" mode)))))

(defun determine-aes-oracle-mode (oracle-fn &key key mode)
  "Using the oracle function, determine what mode the oracle is using. Since we need to test this
  eventually, allow passing the mode through to the oracle."
  (let* ((block-size (determine-aes-oracle-block-size oracle-fn))
         (data (make-byte-vect (* block-size 3)))
         (result (funcall oracle-fn data :key key :mode mode)))
    (loop for block-index from 0 to (- (length result) (* 2 block-size)) by block-size
          when (equalp (subseq result block-index (+ block-index block-size))
                       (subseq result (+ block-index block-size) (+ block-index (* block-size 2))))
            do (return-from determine-aes-oracle-mode :ecb))
    :cbc))

(defun determine-aes-ecb-oracle-prepend-secret-length (oracle-fn starting-block block-size)
  "Determine the length of the aes ecb oracle's prepended secret."
  (loop with previous = (funcall oracle-fn (make-byte-vect block-size))
        ;; target will change when we roll over the border
        with target-start = (* block-size (1+ starting-block))
        with target-end = (+ target-start block-size)
        ;; we know which block to look at so only search along this block
        for sub-block-index from 0 below block-size
        for inject = (let ((vect (make-byte-vect block-size)))
                       (loop for index from 0 to sub-block-index
                             do (setf (aref vect index) 1))
                       vect)
        for result = (funcall oracle-fn inject)
        ;; adjust injection contents at start till known data site contents changed
        while (equalp (subseq previous target-start target-end)
                      (subseq result target-start target-end))
        do (setf previous result)
        ;; prepend distance is how many bytes were changed in injection contents - 1
        finally (return (- (* (1+ starting-block) block-size) sub-block-index))))

(defun determine-aes-ecb-oracle-data-block-index (oracle-fn block-size)
  "Return which blocks, given the block-size, the oracle-fn is placing the data within."
  (let* ((inject-0s (make-byte-vect (* 3 block-size)))
         (inject-1s (make-byte-vect (* 3 block-size) :initial-element 1))
         (result-0s (funcall oracle-fn inject-0s))
         (result-1s (funcall oracle-fn inject-1s)))
    (if (/= (length result-0s) (length result-1s))
        (error "Oracle-fn sending back unpredictable length data")
        ;; Since the results have different injections, only the blocks containing the
        ;; injections should differ between results.
        (loop for block-index from 0 to (- (length result-0s) block-size) by block-size
              while (equalp (subseq result-0s block-index (+ block-index block-size))
                            (subseq result-1s block-index (+ block-index block-size)))
              finally (return (/ block-index block-size))))))

(defun determine-aes-ecb-oracle-secret-length (oracle-fn block-size &optional (prepend-length 0))
  "Determine the length of the appended secret for the aes-ecb-oracle."
  (loop with previous = (funcall oracle-fn #())
        for offset from 1 to block-size
        for check = (funcall oracle-fn (make-byte-vect offset))
        while (= (length previous) (length check))
        do (setf previous check)
        finally (return (1+ (- (length previous) offset prepend-length)))))

(defun determine-aes-ecb-oracle-append (oracle-fn block-size)
  "Using the AES oracle function to represent a remote server, take the block size and determine
  the contents of the block appended to the user sent data."
  (declare (optimize debug))
  (let* ((data-block-index (determine-aes-ecb-oracle-data-block-index oracle-fn block-size))
         (data-start-index (determine-aes-ecb-oracle-prepend-secret-length
                            oracle-fn data-block-index block-size))
         (prepend-offset (- (round-up-to-mod data-start-index block-size) data-start-index))
         (secret-length (determine-aes-ecb-oracle-secret-length
                         oracle-fn block-size data-start-index))
         (secret (make-byte-vect secret-length)))
    (labels ((craft-dict (index next-byte)
               ;; dict consists of the secret with an additional byte added to the end to match
               ;; against the target
               (let* ((length (+ prepend-offset (round-up-to-mod index block-size)))
                      (target (make-byte-vect length)))
                 (copy-byte-vect-range secret 0 (1- index)
                                       target (- length index 1) (1- length))
                 (setf (aref target (1- length)) next-byte)
                 target))
             (craft-target (index)
               ;; target consists of an empty byte vector with the right length such that the
               ;; oracle responds with a portion of the appended secret within the last block
               (let ((length (+ prepend-offset (- block-size 1 (mod index block-size)))))
                 (make-byte-vect length))))
      (loop for secret-index from 0 below secret-length
            for data-index from (+ prepend-offset data-start-index)
            ;; inject a block with the last byte empty
            for inject = (craft-target secret-index)
            ;; grab the final block of the data
            for target-block-start = (round-down-to-mod data-index block-size)
            for target-block-end = (round-up-to-mod data-index block-size)
            for response = (funcall oracle-fn inject)
            for target = (subseq response target-block-start target-block-end)
            ;; compare it to a dictionary of blocks made by the oracle
            do (loop for byte from 0 to 255
                     for dict = (craft-dict secret-index byte)
                     do (let ((oracle-says (funcall oracle-fn dict)))
                          (when (equalp (subseq oracle-says target-block-start target-block-end)
                                        target)
                            (setf (aref secret secret-index) byte)
                            (return))))))
    secret))

(let ((static-key (make-random-byte-vect 16))
      (prepend-to-vect (make-random-byte-vect 24)) ;(+ 96 (random 48))))
      (append-to-vect (b64-string-to-byte-vect "Um9sbGluJyBpbiBteSA1LjAK")));V2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK
  (defun aes-ecb-oracle-prepend-append (vect &key key (mode :ecb))
    "Using the key and mode if given, encrypt the vector using AES. If either the key or mode is not
  given, then use random. Currently only accepts :ecb as the mode."
    (if (or (null mode)
            (eq mode :ecb))
        (let ((padded (byte-vect-list-to-byte-vect (list prepend-to-vect vect append-to-vect))))
          (encrypt-aes-ecb-padded padded (if key key static-key)))
        (error "Mode must be :ecb but was called with ~A" mode)))
  (defun dump-aes-ecb-prepend-oracle-static-key ()
    static-key)
  (defun dump-aes-ecb-prepend-oracle-appended-secret ()
    append-to-vect)
  (defun dump-aes-ecb-prepend-oracle-prepended-secret ()
    prepend-to-vect))

(let ((static-key (make-random-byte-vect 16))
      (append-to-vect (b64-string-to-byte-vect "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK")))
  (defun aes-ecb-oracle (vect &key key (mode :ecb))
    "Using the key and mode if given, encrypt the vector using AES. If either the key or mode is not
  given, then use random."
    (if (or (null mode)
            (eq mode :ecb))
        (let ((padded (byte-vect-list-to-byte-vect (list vect append-to-vect))))
          (encrypt-aes-ecb-padded padded (if key key static-key)))
        (error "Mode must be :ecb but was called with ~A" mode)))
  (defun dump-aes-ecb-oracle-static-key ()
    static-key)
  (defun dump-aes-ecb-oracle-secret ()
    append-to-vect))

(defun determine-aes-oracle-block-size (oracle-fn &optional (type :byte-vector))
  "Given an aes oracle, determine the block size. Will use the type to determine what to send
  the oracle. Current types are strings and byte vectors."
  (loop with previous-result = nil
        for block-size from 1
        for block = (cond ((eq type :byte-vector)
                           (make-byte-vect block-size))
                          ((eq type :string)
                           (make-string block-size))
                          (t (error "Unknown type: ~A" type)))
        for result = (funcall oracle-fn block)
        until (and previous-result
                   (> (length result) (length previous-result)))
        do (setf previous-result result)
        finally (return (- (length result) (length previous-result)))))



;;; attack ecb encoded profiles

(defun key-val-parse (string)
  "Return the alist represented by the string where & separates entries
  and = separates keys from vals"
  (loop with alist = '()
        for pair in (split-string string "&")
        do (let* ((key-and-val (split-string pair "="))
                  (key (first key-and-val))
                  (val (rest key-and-val)))
             (unless (assoc key alist :test #'string=)
               (setf alist (acons key val alist))))
        finally (return (nreverse alist))))

(let ((uid 0)
      (user-alist '()))
  (defun make-user-profile (email)
    "Make a new user profile on the server."
    (let ((email (strip-chars email #\& #\=)))
      (unless (assoc email user-alist :test #'string=)
        (setf user-alist (acons email (list (cons "email" email)
                                            (cons "uid" uid)
                                            (cons "role" "user"))
                                user-alist)))
      (profile-for email)))
  (defun profile-for (email)
    "Given the email, either return the profile if present or create a new one."
    (if (assoc email user-alist :test #'string=)
        (encode-profile (rest (assoc email user-alist :test #'string=)))
        (make-user-profile email))))

(defun encode-profile (profile)
  "Encode the profile using & to separate entries and = to separate keys from vals."
  (loop for (key . val) in profile
        collecting (string-list-to-string (list key "=" (if (stringp val)
                                                            val (format nil "~A" val))))
          into pairs
        finally (return (unsplit-string-list pairs "&"))))

(let ((aes-key (make-random-byte-vect 16)))
  (defun encrypt-profile (email)
    "AES ECB encrypt the profile with padding."
    (encrypt-aes-ecb-padded (string-to-byte-vect (profile-for email)) aes-key))
  (defun decrypt-profile (encrypted)
    "Decrypt the padded profile using AES ECB"
    (key-val-parse (byte-vect-to-string (decrypt-aes-ecb-padded encrypted aes-key)))))
 
  
   
(defun force-role=admin (email)
  "Attack a server using AES ECB to encode profiles, leveraging the block alignment to inject
  admin into the role. Note, this makes several assumptions, such as the role being the last
  entry in the profile and email being the first."
  (let* ((oracle #'encrypt-profile)
         (block-size (determine-aes-oracle-block-size oracle :string))
         (email-length (length email))
         (valid (profile-for email))
         (padded-admin (byte-vect-to-string (pad-vect-pkcs7 (string-to-byte-vect "admin")
                                                            block-size)))
         (role-index (position "=" valid :test #'string= :from-end t))
         (email-position (position "=" valid :test #'string=))
         (cutoff-index (next-pad-size role-index block-size))
         (padded-email-length (+ (- cutoff-index role-index 1)
                                 email-length))
         (padded-email (let* ((splits (split-string email "@"))
                              (pre (first splits))
                              (post (first (rest splits)))
                              (mids (loop repeat (- padded-email-length email-length)
                                          collecting (subseq post 0 1))))
                         (string-list-to-string (append (list pre) (list "@") mids (list post)))))
         (encrypt-to-block-size (encrypt-profile padded-email))
         (removed-user-block (subseq encrypt-to-block-size 0 cutoff-index))
         (admin-injection (let* ((pre-pad-size (- (next-pad-size email-position block-size)
                                                  email-position 1))
                                 (pre-padding (make-byte-vect pre-pad-size)))
                            (string-list-to-string (list (byte-vect-to-string pre-padding)
                                                         padded-admin))))
         (isolated-admin-block (subseq (encrypt-profile admin-injection)
                                block-size (* block-size 2))))
    (byte-vect-list-to-byte-vect (list removed-user-block isolated-admin-block))))

(let ((key (make-random-byte-vect 16))
      (init (make-random-byte-vect 16)))
  (defun encrypt-aes-cbc-bitflip-oracle (string)
    (let ((cleaned-string (concatenate 'string
                                       "comment1=cooking%20MCs;userdata="
                                       (swap-blacklist-chars-for-escape string '(#\= #\;) #\?)
                                       ";comment2=%20like%20a%20pound%20of%20bacon")))
      (encrypt-aes-cbc (pad-vect-pkcs7 (string-to-byte-vect cleaned-string)
                                       (round-up-to-mod (length cleaned-string) 16))
                       key init)))
  (defun decrypt-aes-cbc-bitflip-oracle (vect)
    (unpad-vect-pkcs7 (decrypt-aes-cbc vect key init)))
  (defun dump-aes-cbc-bitflip-key ()
    key))

(defun test-cbc-bitflip-oracle (crypttext)
  (let ((result (byte-vect-to-string (decrypt-aes-cbc-bitflip-oracle crypttext))))
    (loop for chunk in (split-string result ";")
          for chunk-list = (split-string chunk "=")
          when (and (string= "role" (first chunk-list))
                    (string= "admin" (first (rest chunk-list))))
            do (return t)
          finally (return nil))))

(defun prep-aes-cbc-injection (crypttext target-index inject-byte decrypted-byte-at-index block-size)
  (let* ((pre-index (- target-index block-size))
         (pre-entry (aref crypttext pre-index))
         (injection (copy-byte-vect crypttext)))
    (setf (aref injection pre-index)
          (logxor decrypted-byte-at-index inject-byte pre-entry))
    injection))

(defun aes-cbc-force-role=admin ()
  (let* ((inject ";role=admin")
         (encrypted (encrypt-aes-cbc-bitflip-oracle inject))
         (decrypted (decrypt-aes-cbc-bitflip-oracle encrypted))
         (block-size (determine-aes-oracle-block-size #'encrypt-aes-cbc-bitflip-oracle :string))
         (d-string (byte-vect-to-string decrypted))
         (role-index (substring-position "role" d-string))
         (admin-index (substring-position "admin" d-string)))
    (labels ((subbed ()
               ;;encryption is using substitution of the ; and or = characters
               (let* ((semi-index (1- role-index))
                      (equals-index (1- admin-index))
                      (semi-sub (char-code (char d-string semi-index)))
                      (equals-sub (char-code (char d-string equals-index))))
                 (prep-aes-cbc-injection
                  (prep-aes-cbc-injection encrypted semi-index (char-code #\;) semi-sub block-size)
                  equals-index (char-code #\=) equals-sub block-size))))
      (cond ((not (and role-index admin-index))
             (error "Role or admin not present in the decryption result."))
            ((= (- admin-index role-index) 5)
             (subbed))
            ((= (- admin-index role-index) 7)
             (error "currently not able to break quoted role auth cleaning type."))
            (t (error "Unable to determing encryption role-authorization cleaning method."))))))



(let ((key (make-random-byte-vect 16))
      (init (make-random-byte-vect 16))
      (prepend-list '(
                      "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc="
                      "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic="
                      "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw=="
                      "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg=="
                      "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl"
                      "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA=="
                      "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw=="
                      "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8="
                      "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g="
                      "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93")))
  (defun make-aes-cbc-cookie ()
    (values (encrypt-aes-cbc (pad-vect-pkcs7-to-block
                              (string-to-byte-vect
                               (nth (random (length prepend-list)) prepend-list))
                              16)
                             key init)
            init))
  (defun decrypt-aes-cbc-cookie (crypttext)
    (unpad-vect-pkcs7 (decrypt-aes-cbc crypttext key init)))
  (defun verify-aes-cbc-cookie (crypttext)
    (valid-pkcs7 (decrypt-aes-cbc crypttext key init)))
  (defun dump-aes-cbc-cookie-key ()
    key)
  (defun dump-aes-cbc-cookie-init ()
    init))

(defun attack-aes-cbc-cookie-oracle ()
  (let ((oracle-fn #'make-aes-cbc-cookie)
        (verify-fn #'verify-aes-cbc-cookie))
    (multiple-value-bind (crypt init-key)
        (funcall oracle-fn)
      (let* ((length (length crypt))
             (block-size (length init-key))
             (diblock-size (* block-size 2))
             (plaintext (make-byte-vect length))
             (crypt (byte-vect-list-to-byte-vect (list init-key crypt))))
        (labels ((decode-cookie (diblock index plain-index)
                   (loop with pad = (- block-size index)
                         with injection = (make-inject diblock plain-index index pad)
                         for decode in (make-decode-list pad)
                         do (let ((decode-injection (copy-byte-vect injection)))
                              (setf (aref decode-injection index)
                                    (inject-byte decode (aref decode-injection index) pad))
                              (when (funcall verify-fn decode-injection)
                                (return decode)))
                         finally (error "Reached end of decode possibilites without match")))
                 (make-inject (diblock plain-index index pad)
                   (loop with inject = (copy-byte-vect diblock)
                         for inject-index from (1+ index) below block-size
                         do (setf (aref inject inject-index)
                                  (inject-byte (aref plaintext (+ plain-index inject-index))
                                               (aref inject inject-index)
                                               pad))
                         finally (return inject)))
                 (make-decode-list (pad)
                   (loop for pre from 0 below pad collecting pre into list
                      finally (return (append list (loop for post from (1+ pad) to 255
                                                         collecting post into list
                                                         finally (return (append list (list pad))))))))
                 (inject-byte (decrypted-byte encrypted-byte inject-byte)
                   (logxor decrypted-byte encrypted-byte inject-byte)))
          (loop for plain-index from 0 below length by block-size
                do (loop with diblock = (copy-byte-vect-from crypt plain-index diblock-size)
                         for index from (1- block-size) downto 0
                         do (setf (aref plaintext (+ plain-index index))
                                  (decode-cookie diblock index plain-index))))
          (unpad-vect-pkcs7 plaintext))))))














